require('babel-register')
const exec = require('child_process').exec
const eos = require('../integration/eos_connector')

const contractHolder = "ivanver"
const account = "nik"

const path1 = "temp\/q"
const path2 = "gitpub-mvp\/user"

const name1 = path1.substr(path1.lastIndexOf("\/") + 1)
const name2 = path2.substr(path2.lastIndexOf("\/") + 1)

async function sh(cmd) {
  return new Promise(function (resolve, reject) {
    exec(cmd, (err, stdout, stderr) => {
      if (err) {
        reject(err);
      } else {
        stdout = stdout.substring(0, stdout.length - 1)
        resolve({ stdout, stderr });
      }
    });
  });
}

async function push() {
  let { stdout } = await sh(`bash ./tools/push.sh $HOME/${path1}/.git`)
  // let { stdout } = await sh(`bash ./tools/push.sh $HOME/${path2}/.git`)
  
  console.log(stdout)

  eos.transaction({
    actions: [{
      account: contractHolder,
      name: 'addrepo',
      authorization: [{
        actor: account,
        permission: 'active'
      }],
      data: {
        reponame: name1,
        userlink: stdout
      }
    }]
  }).then(res => {
    console.log(res)
  }).catch(err => {
    console.log(err)
  });
}

push()

async function clone(_hash) {
  let hash = _hash || "QmPJGUBbTwoy42Ab5AowS5TRytLs6FUzo8dDPQ6VELFwaG"
  let { stdout } = await sh(`bash ./tools/clone.sh ${hash} /Users/nikbailo/temp`)
}

// clone()

