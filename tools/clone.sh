url="$1"
repo="$2"

ipfs get $url
mkdir $HOME/temp/$url
mkdir $HOME/temp/$url/.git
cp -r $url/ $HOME/temp/$url/.git
rm -rf $url

cd $HOME/temp/$url/.git
git config --local --bool core.bare false
cd ..
git checkout .